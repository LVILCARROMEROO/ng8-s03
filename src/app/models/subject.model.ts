export interface SubjectInterface {
  id: string;
  name: string;
  instructor: string;
  start: Date;
  end: Date;
  vacancies: number;
  active: boolean;
}

export class SubjectModel {
  id: string;
  name: string;
  instructor: string;
  start: Date;
  end: Date;
  vacancies: number;
  active: boolean;

  constructor(data: SubjectInterface) {
    this.id = data.id || null;
    this.name = data.name || null;
    this.instructor = data.instructor || null;
    this.start = data.start || null;
    this.end = data.end || null;
    this.vacancies = data.vacancies || null;
    this.active = data.active || null;
  }
}
