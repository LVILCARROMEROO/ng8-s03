import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'yesno'
})
export class YesnoPipe implements PipeTransform {

  transform(value: any, yes: string = 'Si', no: string = 'No'): any {
    return `<b> ${value ? yes : no} </b>`;
  }

}
