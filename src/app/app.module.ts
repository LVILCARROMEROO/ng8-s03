import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SubjectsPageComponent } from './pages/subjects-page/subjects-page.component';
import { SubjectFormPageComponent } from './pages/subject-form-page/subject-form-page.component';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { DefaultPipe } from './pipes/default.pipe';
import { YesnoPipe } from './pipes/yesno.pipe';

@NgModule({
  declarations: [
    AppComponent,
    SubjectsPageComponent,
    SubjectFormPageComponent,
    DefaultPipe,
    YesnoPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
