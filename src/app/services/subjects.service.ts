import { Injectable } from '@angular/core';
import {SubjectInterface, SubjectModel} from '../models/subject.model';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SubjectsService {
  subjects: SubjectModel[] = [];
  endpoind = environment.API_URL;

  constructor(
    private http: HttpClient
  ) { }

  getAll(): Observable<SubjectModel[]> {
    return this.http.get(`${this.endpoind}/subjects.json`)
      .pipe(
        map((response) => Object.keys(response).map(key => {
          const subject = { id: key, ...response[key]} as SubjectInterface;
          return new SubjectModel(subject);
        }))
      );
  }

  getOne(id): Observable<SubjectModel> {
    return this.http.get(`${this.endpoind}/subjects/${id}.json`)
      .pipe(
        map((response) => {
          const subject = { id, ...response} as SubjectInterface;
          return new SubjectModel(subject);
        })
      );
  }

  create(obj: SubjectInterface) {
    return this.http.post(`${this.endpoind}/subjects.json`, obj)
      .pipe(
        map((response: any) => {
          const subject = { id: response.name, ...obj} as SubjectInterface;
          return new SubjectModel(subject);
        })
      );
  }

  update(id, obj: SubjectInterface) {
    return this.http.put(`${this.endpoind}/subjects/${id}.json`, obj)
      .pipe(
        map((response) => {
          const subject = { id, ...response} as SubjectInterface;
          return new SubjectModel(subject);
        })
      );
  }

  delete(id) {
    return this.http.delete(`${this.endpoind}/subjects/${id}.json`);
  }

}
