import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SubjectsPageComponent} from './pages/subjects-page/subjects-page.component';
import {SubjectFormPageComponent} from './pages/subject-form-page/subject-form-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'subjects', pathMatch: 'full' },
  { path: 'subjects', component: SubjectsPageComponent },
  { path: 'subject-form', component: SubjectFormPageComponent },
  { path: 'subject-form/:id', component: SubjectFormPageComponent },
  { path: '**', redirectTo: 'subjects' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
