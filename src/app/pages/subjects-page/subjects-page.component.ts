import { Component, OnInit } from '@angular/core';
import {SubjectModel} from '../../models/subject.model';
import {SubjectsService} from '../../services/subjects.service';

@Component({
  selector: 'app-subjects-page',
  templateUrl: './subjects-page.component.html',
  styleUrls: ['./subjects-page.component.scss']
})
export class SubjectsPageComponent implements OnInit {
  subjects: SubjectModel[] = [];

  constructor(
    private subjectsService: SubjectsService
  ) { }

  ngOnInit() {
    this.subjectsService.getAll()
      .subscribe(
        res => this.subjects = res,
        err => console.log(err),
      );
  }

  delete(id) {
    this.subjectsService.delete(id)
      .subscribe(
        res => {
          const index = this.subjects.findIndex(subject => subject.id === id);
          this.subjects.splice(index, 1);
        },
        err => console.log(err)
      );
  }

}
