import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {SubjectsService} from '../../services/subjects.service';
import {ActivatedRoute, Router} from '@angular/router';

export const customRequired = (control: FormControl) => {

  if (control.value) {
    return null; // caso de exito
  }

  return { customRequired: 'Este campo es requerido' }; // caso de error
};

@Component({
  selector: 'app-subject-form-page',
  templateUrl: './subject-form-page.component.html',
  styleUrls: ['./subject-form-page.component.scss']
})
export class SubjectFormPageComponent implements OnInit {
  subjectForm: FormGroup;
  id: string;

  get nameField() {
    return this.subjectForm.get('name');
  }
  constructor(
    private fb: FormBuilder,
    private subjectsService: SubjectsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.subjectForm = this.fb.group({
      name: [null, Validators.required],
      instructor: 'Luis Vilcarromero',
      vacancies: null,
      start: null,
      end: null,
      active: null
    });
  }

  ngOnInit() {
    this.subjectsService.getOne(this.id)
      .subscribe(
        res => {
          const { name, instructor, vacancies, start, end, active } = res;
          this.subjectForm.patchValue({
            name, instructor, vacancies, start, end, active
          });
        },
        err => console.log(err)
      );
  }

  save() {
    const values = this.subjectForm.value;
    if (this.id) {
      this.subjectsService.update(this.id, values)
        .subscribe(
          res => {
            console.log(res);
            this.router.navigateByUrl('/subjects');
          },
          err => console.log(err)
        );
    } else {
      this.subjectsService.create(values)
        .subscribe(
          res => {
            console.log(res);
            this.router.navigateByUrl('/subjects');
          },
          err => console.log(err)
        );
    }

  }

}



