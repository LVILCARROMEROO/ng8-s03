import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectFormPageComponent } from './subject-form-page.component';

describe('SubjectFormPageComponent', () => {
  let component: SubjectFormPageComponent;
  let fixture: ComponentFixture<SubjectFormPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectFormPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectFormPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
