import { Component } from '@angular/core';
import {tryCatch} from 'rxjs/internal-compatibility';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Angular 8 - clase 3';
  subject = { name: 'Angular 8', instructor: 'Luis Vilcarromero' };
  today = new Date();
  description = 'abcdefghijabcdefghij';
  // promise: Promise<boolean>;
  constructor() {
    // callback
    /*
    const function2 = () => {};
    const function1 = (callback) => {
      callback();
    };

    function1(function2);
    */
    /*
    this.promise = new Promise<boolean>((resolve, reject) => {
      console.log('promise1');
      try {
        resolve(true);
      } catch {
        reject(false);
      }
    });
    */

  }

}
